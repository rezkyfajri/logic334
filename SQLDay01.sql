--SQL day 01

--DDL (data defenition lenguange)
--Buat DataBase
create database db_kampus
use db_kampus
--Hapus database
drop database db_kampus

create table mahasiswa( 
id bigint primary key identity (1,1),
name varchar(50) not null, 
address varchar(50) not null,
email varchar(225) null
)

--create view (membuat database virtual)
create view vw_mahasiswa as select * from mahasiswa
--
select * from vw_mahasiswa

--ALTER 
--nambahin column
alter table mahasiswa add nomor_hp varchar(100) not null

--hapus column
alter table mahasiswa drop column nomor_hp

--alter column 
alter table mahasiswa alter column email varchar (100) not null


--Drop
--hapus (drop)
--hapus database
drop database [dataBaseName]
--hapus table
drop table [tableName]
--hapus view
drop view [viewName]

--DML (Data manipulation lenguage)

--INSERT 

insert into mahasiswa(name , address, email) 
values 
('Haikal','Kuningan ', 'haikal5nsah@gmail.com'),
('Imam', 'Bekasi', 'imamassidqi@gmail.com'),
('Irvan','Jakarta','aliirvan122@gmail.com'),
('Tunggul','Semarang','Tunggulyudhaputra5@gmail.com'),
('sabrina','Palembang','shabrinaputrif1604@gmail.com'),
('test ','Palembang','shabrinaputrif1604@gmail.com')

--Select (pake dbo kalo beda database)
select id, name, address, email from dbo.mahasiswa
 --Update
Update mahasiswa set name = 'Sabrina' where id = 6
update mahasiswa set name = 'Haikal Firmansyah' where id = 2

 --delete 
delete mahasiswa where name = 'Test'



--TOP
--nampilin hasil teratas berdasarkan
select top 3 * from dbo.mahasiswa 






create table biodata (
id bigint primary key identity (1,1),
mahasiswa_id bigint null,
tgl_lahir date null,
gender varchar (10) null,
)

select * from biodata

insert into biodata (mahasiswa_id, tgl_lahir, gender)
values 
(1,'2020-06-10','pria'),
(1,'2021-07-10','pria') ,
(1,'2022-08-10','wanita')

update biodata set mahasiswa_id= 3 where tgl_lahir = '2022-08-10'
update biodata set mahasiswa_id= 3 where tgl_lahir = '2021-07-10'
--JOIN (AND, OR, NOT)
select mhs.id as id_mahasiwa , mhs.name as nama_mahasiswa, mhs.address as alamat,
mhs.email as email, bio.tgl_lahir as tanggal_lahir, bio.gender as gender
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.id >=1 and mhs.id <=4
order by nama_mahasiswa asc

--TOP
select top 2 mahasiswa_id from biodata order by mahasiswa_id desc

--between 
select * from biodata where mahasiswa_id between 2 and 3
select * from biodata where mahasiswa_id >=2 and mahasiswa_id<=3
select * from biodata where tgl_lahir between '2019-01-01' and '2023-01-01'

--LIKE4
--select * from mahasiswa
--where name like 'a%' --awalanya A
--where name like '%a' --akhiran A
--where name like '%ez%' --bebeas yang penting ada ez
--where name like '_a%' --huruf kedua A
--where name like 'a__%' --awalanya A minimal ada 2 huruf setelah A
--where name like 'a%n' --awalanya A akhiran N

--Group by
select name  from mahasiswa group by name
select * from mahasiswa

--Agregate function (sum count avg min max)
select sum(id) from mahasiswa, name, biodata from mahasiswa group by name, address

insert into mahasiswa (name, address,email)
values ('rezky fajri' ,' Padang' , ' test1234@gmail.com')

--Having 
use db_kampus

--having (pengganti where jika pake group by)
--kalo mau pake where tarok di sebelum group by
--group by digunain kalo ada operasi matematika(sum count avg min max)
select sum(id) as jumlah, name from mahasiswa
group by name
having sum(id) >2
