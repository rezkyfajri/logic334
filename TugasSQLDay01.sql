--TugasSQLDay01
create database DBPenerbit
use DBPenerbit
--Nomor 1 Buat tabel pengarang
create table tblPengarang (
id int primary key identity (1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
Kota varchar (15) not null,
Kelamin varchar (1) not null
)

--Nomor 2 Masukkan Data Tabel pengarang
insert into tblPengarang (Kd_Pengarang,Nama,Alamat,Kota,Kelamin)
values 
('P0001', 'Ashadi', 'Jl.Beo 25','Yogya','P'),
('P0002', 'Rian', 'Jl.Beo 123','Yogya','P'),
('P0003', 'Suwadi', 'Jl.Semangka 13','Bandung','P'),
('P0004', 'Siti', 'Jl.Durian 15' , 'Solo','W'),
('P0005', 'Amir', 'Jl.Gajah 33','Kudus','P'),
('P0006', 'Suparman', 'Jl.Hariamau 25','Jakarta','P'),
('P0007', 'Jaja', 'Jl.Singa 7','Bandung','P'),
('P0008', 'Saman', 'Jl.Naga 12','Yogya','P'),
('P0009', 'Anwar', 'Jl.Tidar 6A','Magelang','P'),
('P0010', 'Fatmawati', 'Jl.Renjana 4','Bogor','W')
select * from tblPengarang


--nomor 1 Buat Tabel Gaji
create table tblGaji (
id int primary key identity (1,1),
Kd_pengarang varchar (7) not null,
Nama varchar (30) not null,
Gaji decimal (18,4) not null
)


--Nomor 2 Masukkan Data Tabel Gaji
insert into tblGaji(Kd_pengarang, Nama, Gaji)
values
('P0002', 'Rian' , 600000),
('P0005', 'Amir' , 700000),
('P0004', 'Siti' , 500000),
('P0003', 'Suwadi' , 1000000),
('P0010', 'Fatmawati' , 600000),
('P0008', 'Saman' , 750000)
select * from tblGaji

--Nomer 1 Hitung dan Tampilkan Jumlah Pengarang Dari Tabel Pengarang
select COUNT(id) as jumlah from tblPengarang

--Nomer 2 Hitung jumlah Pengrang wanita dan jumlah pengrang pria
select count(id) as jumlah_wanita from tblPengarang
where tblPengarang.Kelamin = 'W'
select count(id) as jumlah_pria from tblPengarang
where tblPengarang.Kelamin = 'P'

--Nomer 3 tampilkan Record kota dan jumlah kotanya dari tblPengrang
select count(id) as jumlah_kota, Kota  from tblPengarang 
group by Kota

--Nomer 4 Tampilkan record Kota diatas 1 kota dari tblPengarang
select count(id) as jumlah_kota, Kota  from tblPengarang 
group by Kota
having COUNT(id) > 1

--Nomer 5 tampilkan Kd_pengrang yang terbesar dan terkecil dari tabel pengrang
select Top 1 Kd_Pengarang from tblPengarang
order by Kd_Pengarang asc 
select 1 Kd_Pengarang from tblPengarang
order by Kd_Pengarang desc 

--Nomer 6 tampilkan Gaji tertinggi dan terendah
select Top 1 Gaji from tblGaji
order by Gaji asc
select Top 1 Gaji from tblGaji
order by Gaji desc

--Nomer 7 tampilkan Gaji diatas 600000
select Gaji from tblGaji
where Gaji > 600000

---- Nomer 8 tampilkan jumlah gaji
select sum(Gaji) as jumlah from tblGaji


--Nomer 9 Tampilkan Gaji berdasarkan Kota
Select sum(Gaji) as total_Gaji,Kota from tblGaji
inner Join tblPengarang  on tblGaji.Kd_pengarang = tblPengarang.Kd_Pengarang
group by Kota 

--Nomer 10 Tampilkan Seluruh Record Pengrang Antara P0003 - P0006 dari tabel pengrang
select * from tblPengarang 
where Kd_Pengarang between 'P0003' and 'P0006'

--Nomer 11 Tampilkan Seluruh Data Yogya Solo dan magelang dari tabel Pengarang
select * from tblPengarang
where Kota = 'Yogya' Or Kota = 'Magelang' Or Kota='Solo'

--Nomer 12 tampilkan seluruh data yang bukan Yogya dari tabel pengrang
select * from tblPengarang
where not Kota= 'yogya'

--Nomer 13 Tampilkan seluruh data pengrang yang nama
--A. dimulai Huruf A
select * from tblPengarang
where nama like 'a%'

--B. berakhiran i
select * from tblPengarang
where nama like '%i'

--C. Huruf Ketiga nya A
select * from tblPengarang
where nama like '__a%'

--D.tidak berakhiran n
select * from tblPengarang
where not nama like '%n'

--Nomer 14 Tampilkan Seluruh Data Table tblPengarang dan tblGaji dengan KD_Pengrang yang sama
select * from tblPengarang
inner join tblGaji on tblPengarang.Kd_Pengarang = tblGaji.Kd_pengarang

--Nomer 15 Tampilkan Kota yang Memiliki gaji dibawah 1.000.000
select kota, Gaji from tblPengarang
inner join tblGaji on tblGaji.Kd_pengarang = tblPengarang.Kd_Pengarang
where Gaji <1000000

--Nomer 16 Ubah Panjang dari Tipe kelamin Menjadi 10
alter table tblPengarang 
alter column Kelamin varchar (10) not null

--NOmer 17 Tambahkan Kolom Gerlar dengan tipe varchar (12) pada tablePengran
alter table tblPengarang add Gelar varchar(12) null

--Nomer 18 Ubah alamat dan kota dari rian
--Menjad Jl Cendrawasih 65 dan kota pekan baru
update tblPengarang set Alamat ='Jl.Cendrawasih 65', 
Kota = 'PekanBaru' where Nama ='rian'

--Nomer 19 buat view untuk dengan nama vwPengarang
create view vwPengarang as select 
tblPengarang.Kd_Pengarang,
tblPengarang.Nama,
tblPengarang.Kota,
tblGaji.Gaji
from tblPengarang
left Join tblGaji
on tblPengarang.Kd_Pengarang = tblGaji.Kd_pengarang

