﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml.Schema;

Tugas5();
static void Tugas1()
{
    Console.WriteLine("Lingkaran");
    Console.Write("Masukkan Jari-jari Lingkaran : ");
    int r = int.Parse(Console.ReadLine());
    //const float phi = (float)Math.PI;
    float K = 2 * (float)Math.PI * r;
    float L = (float)Math.PI * r * r;
    Console.WriteLine("jumlah keliling lingkarang adalah = " + Math.Round (K,0));
    Console.WriteLine("jumlah Luas lingkaran adalah = " + Math.Round(L,0));
}

static void Tugas2()
{
    Console.WriteLine("Persegi");
    Console.Write("masukkan panjang sisi : ");
    int s = int.Parse(Console.ReadLine());
    int L = s * s;
    int K = 4*s;
    Console.WriteLine("jumlah keliling persegi adalah = " + K);
    Console.WriteLine("jumlah Luas persegi adalah = " + L);
}

static void Tugas3()
{
    Console.WriteLine("Modulus");
    Console.Write("masukkan Angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    int Hasil = angka % pembagi;
   //    if Hasil == 0 ? "angka" + angka + "%" + pembagi + "adalah 0" : "angka" + angka + "%" + pembagi + "bukan 0 melainkan "+Hasil;
    if (Hasil == 0)
    {
        Console.WriteLine($"angka {angka} % {pembagi} adalah 0");
    }
    else
    {
        Console.WriteLine($"angka {angka} % {pembagi} bukan 0 melainkan {Hasil}");
    }
}

static void Tugas4()
{
    Console.WriteLine("Tugas 4");
    Console.WriteLine("8 puntung rokok sama dengan 1 batang rokok");
    Console.WriteLine("1 Batang Rokok dihargai Rp.500");
    Console.Write("Jumlah Puntung Rokok yang diambil : ");
    int puntungRokok = int.Parse(Console.ReadLine());
    
    int puntungToRokok = 8;
    int hargaRokok = 500;
    int rokok = puntungRokok / puntungToRokok;
    int sisaPuntung = puntungRokok % puntungToRokok;
    int uang = rokok * hargaRokok;

    Console.WriteLine("Hasil");
    Console.WriteLine("total puntung rokok yang didapat: " + puntungRokok);
    Console.WriteLine("Total Rokok yang bisa dibuat : " + rokok + " (jawaban bagian 1)");
    Console.WriteLine($"sisa puntung rokok yang ada : {sisaPuntung}");
    Console.WriteLine($"uang yang dihasilkan dari menjual rokok : Rp.{uang}- (jawaban bagian 2)");
}

static void Tugas5()
{
    Console.WriteLine("Tugas 5");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    string grade= "";
    if (nilai >= 80  && nilai <=100) 
    {
        grade = "A";
    }
    else if (nilai >= 60 && nilai <=80)
    {
        grade = "B";
    }
    else if (nilai >= 0 && nilai <=60)
    {
        grade = "C";
    }
    else
    {
        Console.WriteLine("Tolong masukkan angka 0 sampai 100");
    }
    if (grade!= "" )
    {
        Console.WriteLine($"Grade Anda Adalah : {grade}");
    }


}
static void Tugas6()
{
    Console.WriteLine("Tugas 6");
    Console.Write("masukkan Angka Ganjil/Genap : ");
    int angka = int.Parse(Console.ReadLine());
    int hasil = angka % 2;
    string kesimpulan = hasil > 0 ? "Ini adalah Bilangan Ganjil" : "Ini Adalah Bilangan genap";
    Console.WriteLine(kesimpulan);
}