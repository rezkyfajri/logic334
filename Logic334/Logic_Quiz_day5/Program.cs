﻿

using System.ComponentModel.Design;

No8();
Console.ReadKey();

static void No1()
{
    Console.WriteLine("Soal Nomor 1");
    Console.WriteLine("Gaji Karyawan berdasarkan Golongan");
    Console.Write("Masukkan Golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    int maxJamKerja = 40;
    int upahPerjam = 0;
    int upah = 0;
    double lembur = 0;


    if (golongan > 0 && golongan < 5)
    {

        Console.Write("Masukkan Jam Kerja : ");
        int jamKerja = int.Parse(Console.ReadLine());


        if (golongan == 1)
        {
            upahPerjam = 2000;
        }
        else if(golongan == 2)
        {
            upahPerjam = 3000;
        }
        else if (golongan == 3)
        {
            upahPerjam = 4000;
        }
        else
        {
            upahPerjam = 5000;
        }
        


        upah = upahPerjam * Math.Min(jamKerja, maxJamKerja);
        if (jamKerja > maxJamKerja)
        {
            jamKerja = jamKerja - maxJamKerja;
            lembur = jamKerja * (upahPerjam * 1.5);
        }
        double total = upah + lembur;

        Console.WriteLine("Upah : \t\t" + upah);
        Console.WriteLine($"lembur : \t {lembur}");
        Console.WriteLine($"Total :\t\t {total}");
    }
    else
    {
        Console.WriteLine("Golongan tidak Valid");
    }   
}

static void No2()
{
    Console.WriteLine("Soal Nomer 2");
    Console.Write("Masukkan Kata : ");
    string input = Console.ReadLine();
    string[] kataKata = input.Split(" ");
    
        int nomer = 0;
    for(int i = 0;i < kataKata.Length;i++)
    {
        if (kataKata[i]== "")
        {
            continue;
        }
        nomer += 1;
        Console.WriteLine($"Kata {nomer} = {kataKata[i]}");
        

    }
    Console.WriteLine("Total Kata adalah : " +nomer);
}

static void No3()
{
    Console.WriteLine("Soal Nomer 3");
    Console.Write(" input : ");
    string input = Console.ReadLine();

    string[] kataKata = input.Split(" ");
    for (int i = 0; i < kataKata.Length; i++)
    {
        for (int j = 0; j < kataKata[i].Length; j++)
        {
            if(j==0 || j == kataKata[i].Length - 1)
            {
                Console.Write(kataKata[i][j]);
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.Write(" ");
    }
}

static void No4()
{

    {
        Console.WriteLine("Soal Nomer 4");
        Console.Write(" input : ");
        string input = Console.ReadLine();

        string[] kataKata = input.Split(" ");
        for (int i = 0; i < kataKata.Length; i++)
        {
            for (int j = 0; j < kataKata[i].Length; j++)
            {
                if (j == 0 || j == kataKata[i].Length - 1)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write(kataKata[i][j]);
                }
            }
            Console.Write(" ");
        }
    }
}


static void No5()
{

    {
        Console.WriteLine("Soal Nomer 5");
        Console.Write(" input : ");
        string input = Console.ReadLine();

        string[] kataKata = input.Split(" ");
        for (int i = 0; i < kataKata.Length; i++)
        {
            for (int j = 0; j < kataKata[i].Length; j++)
            {
                if (j == 0)
                {
                    continue;
                }
                else
                {
                    Console.Write(kataKata[i][j]);
                }
            }
            Console.Write(" ");
        }
    }
}

static void No6()
{
    Console.WriteLine("Soal no 6");
    Console.Write("Masukkan Jumlah (N): ");
    int jumlah= int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Awal : ");
    int nilaiAwal = int.Parse(Console.ReadLine());
    Console.Write("Masukkan kelipatan : ");
    int kelipatan = int.Parse(Console.ReadLine());


    for (int i = 0;i < jumlah; i++)
    {

        int mod = i % 2;
        if (mod == 0)
        {
            Console.Write(nilaiAwal + " ");
        }
        else
        {
            Console.Write("* ");
        }
        nilaiAwal = nilaiAwal * kelipatan;
    }
}

static void No7()
{
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    int[] array = new int[input];
   // Console.Write("INput 1 : ");
    //array[0] = int.Parse(Console.ReadLine());
    array[0] = 1;
    array[1] = 1;

    for (int i = 0; i < array.Length; i++)
    {  
        if (i >= 2)
        {
            array[i] = array[i - 1] + array[i - 2];
        }
       Console.Write(array[i]);

        if (i != array.Length-1)
        {
            Console.Write(",");
        }
            
    }
}


static void No8()
{
    Console.Write("input : ");
    int input = int.Parse(Console.ReadLine());
    for (int i = 1; i<=input; i++)
    {
        for (int j= 1;j<=input ; j++)
        {
            if (i==1) {
                Console.Write(j);
            }
            else if (i == input)
            {
                Console.Write(i +1 - j);
            }
            else if (j==1 || j==input)
            {
                Console.Write("*");
            }
            else
            {
                Console.Write(" ");
            }
        }
            Console.WriteLine();
    }
}