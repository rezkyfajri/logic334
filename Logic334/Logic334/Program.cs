﻿//Output
Console.WriteLine("Hello, World!"); // untuk menulis dengan tambahan enter
Console.Write("Hello guys"); // baris masih sama
Console.WriteLine();


//input
Console.Write("Inputkan nama anda : ");
string nama = Console.ReadLine();
Console.Write("input umur : ");
int umur = 19;
//int tidak bisa di inputkan seperti string, harus konveksi
Console.WriteLine(umur.ToString()); //guna ToString mengubah yg belum string menjadi string

//cara cara menggabungkan string
Console.WriteLine("nama saya {0} dengan umur {1} ", nama, umur);
Console.WriteLine("nama saya" + nama + "dengan  umur " + umur);
Console.WriteLine($"nama saya {nama} dengan umur {umur}");


//variable mutable (variabel yg bisa dirubah)
int dataAngka;
bool dataFalseAtauTrue;
float dataBerkoma;


//variable imutable (variabel yg tidak dirubah) pake const kaya java!
const double phi = 3.14;




Console.ReadKey();
