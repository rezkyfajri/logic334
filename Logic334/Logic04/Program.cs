﻿


//initialisasiArray();

using Logic04;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

//aksesElementArray();
//Array2Dimensi();
//inisialList();
//panggilClassStudent();
//mengaksesElementList();
//insertList();
//indexElementList();
//inisialisasiDateTime();
//parsingDateTime();
//dateTimeProperties();
//timeSpan();
test1();
Console.ReadKey();

static void test1()


{
    char a = 'A';
    char b = 'B';
    Console.WriteLine((int)a + (int) b);
}


static void List2Dimensi()
{
    Console.WriteLine("--List 2 Dimensi--");
    List<List<int>> list = new List<List<int>>()
    {
        new List<int>() { 1, 2, 3 },
        new List<int>() { 4, 5, 6 },
        new List<int>() { 7, 8, 9 }
    };

    //tambah data
    list.Add(new List<int>() { 10, 11, 12 });

    //ubah data
    list[0][1] = 22; //ubah nilai 2 menjadi 22

    //hapus data
    list.RemoveAt(3); //hapus pakai index
    //list.Remove(list[3]); //hapus pakai value

    for (int i = 0; i < list.Count; i++)
    {
        for (int j = 0; j < list[i].Count; j++)
        {
            Console.Write(list[i][j] + "\t");
        }
        Console.WriteLine();
    }
}

static void timeSpan()

{
    


    Console.WriteLine("Time Span");
    DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 30);

    //mencari interval waktu
    TimeSpan interval = date2 - date1;
    Console.WriteLine("Beda Hari : " + interval.Days);
    Console.WriteLine("Total Nomor hari : " + interval.TotalDays);

    Console.WriteLine("Beda jam : " + interval.Hours);
    Console.WriteLine("Total Nomor jam : " + interval.TotalHours);


    Console.WriteLine("Beda menit : " + interval.Minutes);
    Console.WriteLine("Total Nomor menit : " + interval.TotalMinutes);

    Console.WriteLine("Beda detik : " + interval.Seconds);
    Console.WriteLine("Total Nomor detik : " + interval.TotalSeconds);

    Console.WriteLine("Beda milidetik : " + interval.Milliseconds);
    Console.WriteLine("Total Nomor milidetik : " + interval.TotalMilliseconds);

}

static void dateTimeProperties()
{
    Console.WriteLine("DateTime Properties");
    DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

    int year = date.Year;
    int month = date.Month; 
    int day = date.Day;
    string dayString = date.DayOfWeek.ToString();
    string dayString2 = date.ToString("ddd");
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekday = (int)date.DayOfWeek;

    Console.WriteLine("tahun " +year);
    Console.WriteLine("Bulan : " +month);
    Console.WriteLine("tanngal : "+day);
    Console.WriteLine(dayString);
    Console.WriteLine(dayString2);
    Console.WriteLine(jam);
    Console.WriteLine(menit);
    Console.WriteLine(detik);
    Console.WriteLine(weekday);
}

static void parsingDateTime()
{
    Console.WriteLine("Parsing Date Time");
    string dateString = "30/06/2023";
    DateTime date = DateTime.Parse(dateString);



    Console.Write("Masukkan tanngal d/M/yyyy :");
    string dateString1 = Console.ReadLine();
    try
    {
        DateTime date1 = DateTime.ParseExact(dateString1, "d/M/yyyy", null);
        Console.WriteLine(date1);
    }
    catch(Exception e)
    {
        Console.WriteLine("Format yang dimasukkan salah");
        Console.WriteLine($"pesan eror : {e.Message}");
    }
}

static void inisialisasiDateTime()
{
    Console.WriteLine("Inisialisasi Waktu dan Tanggal");
    DateTime dt = new DateTime(); //default waktu 1 januari tengah malam
    DateTime dtNow = DateTime.Now; //tanggal dan waktu sekarang
    DateTime dtCostum = new DateTime(1999, 10, 08, 04,30,00);
    DateTime dtTimeDoang = DateTime.Now;
    DateTime dtDateDoang = DateTime.Now;
    Console.WriteLine(dt);
    Console.WriteLine(dtNow);
    Console.WriteLine(dtCostum);
    Console.WriteLine(dtDateDoang.ToString("dd/MMMM/yyyy")); //MM = nomor bulan, MMM nama bulan dalam singkatan, MMMM nama bulan full berlaku untuk day juga jadi hari
    Console.WriteLine(dtTimeDoang.ToString("hh:mm:ss")); //MM = nomor bulan, MMM nama bulan dalam singkatan, MMMM nama bulan full

}
static void indexElementList()
{
    Console.WriteLine("INdex Element List");
    List<string> list = new List<string>(); 
    list.Add("1");
    list.Add("2");
    list.Add("3");
    Console.Write("Masukkan item yang dicari : ");
    string item = Console.ReadLine();
    int index = list.IndexOf(item);
    if (index != -1)
    {
        Console.WriteLine($"Element {item} is found at {index}");
    }
    else {
        Console.WriteLine($"Element {item} is not Found!");
            }
}


static void insertList()
{
    Console.WriteLine("Insert List");
     List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(1, 4);

    for (int i = 0;i < list.Count; i++) Console.WriteLine(list[i]);
}

static void mengaksesElementList()
{
    Console.WriteLine("Mengakses Element List");
    
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);
    foreach (int i in list) Console.WriteLine(i);
    for (int i = 0;i<list.Count;i++) Console.WriteLine(i);
}


static void panggilClassStudent()
{
    Console.WriteLine("Cara Panggil Class");

    //cara panggil menggunakan import 
    //pake CTR + (titik) untuk shortcut import nya
             // Student student = new Student();
    //cara tanpa import menggunakan nama file
             //Logic04.Student student = new Student();

    List<Student> students = new List<Student>()
    {
        new Student() {Id = 1, Name = "John Doe" },
        new Student() {Id = 2, Name = "Jane" },
        new Student() {Id = 3, Name = "Doe" }
    };
    //tambah data
    //cara satu
    Student student = new Student();
    student.Id = 5;
    student.Name = "Bambang";
    students.Add(student);

    //cara dua
    students.Add(new Student() { Id = 4, Name = "Jamet" });
    // panggil data class dengan loop dengan foreach
    foreach(Student item in students)
    {
        Console.WriteLine($"Student ID: {item.Id}, Student Name : {item.Name}");
    }
    //panggil data class degngan loop biasa
    for ( int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id : {students[i].Id}, Nama : {students[i].Name}" );
    }



    Console.WriteLine($"banyak data : {students.Count}");
}


static void inisialList()
{
    Console.WriteLine("inisilisasi List dan penggunaan nhya");
    List<string> list = new List<string>()
    {
        "rezky fajri",
        "fajri rezky",
        "rezky rezky",
        "untuk dihapus",
        "untuk dihapus2"
    };
    //manmabah data



}

static void Array2Dimensi()
{
    Console.WriteLine("Array 2 dimensi");
    int[,] test = new int[3, 3]
    { 
        {1,2,3 },
        {4,5,6}, 
        {7,8,9} 
    };

    for(int i=0; i < test.GetLength(0); i++)
    {
        for (int j =0; j < test.GetLength(1);j++)
        {
            Console.Write(test[i,j] + " ");
        }
        Console.WriteLine();
    }
}

static void aksesElementArray()
{
    Console.WriteLine("Akses Elemen Array");
    int[] array = new int[3] { 1, 2, 3 };

    //get data
    Console.WriteLine(array[0]);
    Console.WriteLine(array[1]);
    Console.WriteLine(array[2]);

    for(int i=0;i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }


    string[] stringArray = { "rezky fajri", "fajri yoswara", "fajri rezky" };
    foreach(string a in stringArray)
    {
        Console.WriteLine(a);
    }
 
}


static void initialisasiArray()
{
    Console.WriteLine("Inisiasi Array ");
    //Caara pertama
    int[] array = new int[3];
    //Cara Kedua
    int[] array1 = new int[3] { 1, 2, 3 };
    //cara Ketiga
    int[] array2 = new int[] {1,2,3 };
    //cara Keempat 
    int[] array3 = {1,2,3};
    //cara Kelima
    int[] array4;
    array4 = new int[3] { 1, 2, 3 };

    Console.WriteLine(string.Join(", ", array));
    Console.WriteLine(string.Join(", ", array1));
    Console.WriteLine(string.Join(", ", array2));
    Console.WriteLine(string.Join(", ", array3));
    Console.WriteLine(string.Join(", ", array4));
}