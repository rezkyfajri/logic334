﻿

using Logic06;


//objectClass();
//construcktor();
//abstrackClass();
//encapsulation();
overriding();
Console.ReadKey();


static void overriding()
{
    Console.WriteLine("Overriding");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine($"kucing {kucing.pindah()}");
    Console.WriteLine($"paus {paus.pindah()}");

}

static void inheritance()
{
    Console.WriteLine("Inheritance");
    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Supra();
}

static void encapsulation()
{
    Console.WriteLine("Encapsulation");
    persegiPanjang pp = new persegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;
    pp.tampilkanData();

}
static void construcktor()
{
    Console.WriteLine("Constructor");
    Mobil mobil = new Mobil("BA 131 KAU");
    string getPlatno = mobil.getPlatno();
    Console.WriteLine(getPlatno);
}

static void objectClass()
{
    Console.WriteLine("Object Class");
    Mobil mobil = new Mobil("BA 1414 FA");
    mobil.nama = "Toyota Supra";
    mobil.kecepatan = 0;
    mobil.bensin = 10;
    mobil.posisi = 0;

    mobil.percepatan();
    mobil.maju();
    mobil.isiBensin(10);
    mobil.utama();


    Mobil mobil1 = new Mobil("BA 131 KAU");
    mobil1.nama = "Toyota Supra";
    mobil1.kecepatan = 0;
    mobil1.bensin = 10;
    mobil1.posisi = 0;
    mobil1.utama();
}
static void abstrackClass()
{
    Console.WriteLine("Abstrak Class");
    Console.Write("Masukkan X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Y :");
    int y = int.Parse(Console.ReadLine());

    tesTurunan calc = new tesTurunan();


    int jumlah = calc.jumlah(x, y);
    int kurang = calc.kurang(x, y);


    Console.WriteLine($"hasil dari {x} + {y} = {jumlah}");
    Console.WriteLine($"hasil dari {x} - {y} = {kurang}");
}