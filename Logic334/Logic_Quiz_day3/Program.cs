﻿using System.Text.RegularExpressions;
using System.Xml.Schema;

No5();


static void No1()
{
    Console.WriteLine("Membuat Program untuk menentukan Grade Nilai");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    string grade = "";
    if (nilai > 100 || nilai < 0)
    {
        grade = "Masukkan angka yang valid";
    }
    else if (nilai >= 90)
    {
        grade = "Grade adalah : A";
    }else if (nilai >= 70)
    {
        grade = "Grade adalah : B";
    }else if (nilai >= 50)
    {
        grade = "Grade adalah : C";
        
    }
    else
    {
        grade = "Grade adalah : D";
    }

    Console.WriteLine(grade);
}

static void No2()
{
    Console.WriteLine("Program Point pada pembelian pulsa");
    Console.Write("Masukkan Jumlah pulsa yang dibeli : ");
    int pulsa = int.Parse(Console.ReadLine());
    int point = 0;
    if (pulsa < 0)
    {
        point = 0;
    }
    else if(pulsa < 25000)
        {
        point += 80;
    }
        else if (pulsa < 50000)
    {
        point += 200;
    }
    else if (pulsa < 100000)
    {
        point += 400;
    }
    else
    {
        point += 800;
    }


    if (pulsa > 0)
    {
        Console.WriteLine($"Pulsa :\t {pulsa}");
        Console.WriteLine($"Point :\t {point}");
    }
    else
    {
        Console.WriteLine("Tolong masukkan angka yang valid!");
    }
}

static void No3()
{
    Console.WriteLine("GrabFood");
    Console.Write("Masukkan Total Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jarak (KM) : ");
    int jarak = int.Parse(Console.ReadLine());
    int totalPromo = 0;
    if (belanja >= 30000)
    {
        Console.Write("Masukkan Kode Promo : ");
        string kodePromo = Console.ReadLine().ToUpper();
        if (kodePromo == "JKTOVO")
        {
            totalPromo = belanja * 40 / 100;
        }
    }
    //maksimal diskon
    if(totalPromo > 30000)
    {
        totalPromo = 30000;
    }
    int ongkir = 5000;

    if (jarak > 5)
    {
        jarak -= 5;
        ongkir += (jarak * 1000);
    }

    int total = belanja - totalPromo + ongkir;

    //output
    Console.WriteLine("--------------------------");
    Console.WriteLine("Bill");
    Console.WriteLine($"Belanja : \t{belanja}");
    if (belanja >= 30000)
    {
        Console.WriteLine($"Diskon 40% : \t{totalPromo}");
    }
    Console.WriteLine($"Ongkir : \t{ongkir}");
    Console.WriteLine("--------------------------");
    Console.WriteLine($"Total Belanja : \t{total}");
}


static void No4()
{
    Console.WriteLine("Promo Marketplace Shoope");
    Console.Write("Masukkan jumlah Belanja : Rp.");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Ongkos Kirim : Rp.");
    int ongkos  = int.Parse(Console.ReadLine());

    if (belanja >= 30000) 
    {
        Console.WriteLine("1. Min Order 30rb free onkir 5rb dan potongan harga belanja 5rb");
    }if (belanja >= 50000) 
    {
        Console.WriteLine("2. Min Order 50rb free onkir 10rb dan potongan harga belanja 10rb");
    }if (belanja >= 100000) 
    {
        Console.WriteLine("3. Min Order 100rb free onkir 20rb dan potongan harga belanja 10rb");
    }

    int diskonOngkir = 0;
        int diskonBelanja = 0;
    if (belanja >= 30000)
    {
        Console.Write("Masukkan Pilihan Voucher : ");
        int voucher = int.Parse(Console.ReadLine());
        if (voucher == 1)
        {
            diskonOngkir = 5000;
            diskonBelanja = 5000;
        }
        else if (voucher == 2)
        {
            diskonOngkir = 10000;
            diskonBelanja = 10000;
        }
        else if (voucher == 3)
        {
            diskonOngkir = 20000;
            diskonBelanja = 10000;
        }
    }
    //output
    Console.WriteLine("-------------");
    Console.WriteLine($"Belanja : \t {belanja}");
    Console.WriteLine($"Ongkos Kirim : \t{ongkos}");
    if (belanja >= 30000)
    {
        Console.WriteLine($"Disko Ongkir : \t{diskonOngkir}");
        Console.WriteLine($"Diskon Belanja : \t{diskonBelanja}");
    }

    ongkos = ongkos - diskonOngkir;
    if(ongkos <0)
    {
        ongkos =0;
    }
    belanja -= diskonBelanja;
    int totalBelanja = belanja + ongkos;
    Console.WriteLine($"Total Belanja : \t{totalBelanja}");

}

static void No5()
{
    Console.WriteLine("Program Cetak nama dan generasi");
    Console.Write("Masukkan Nama Anda : ");
    string nama = Console.ReadLine();
    Console.Write("Masukkan tahun lahir : ");
    int tahunLahir= int.Parse(Console.ReadLine());
    if(tahunLahir < 1944)
    {
        Console.WriteLine("Terlalu tua");
    }
    else if(tahunLahir <= 1964)
    {
        Console.WriteLine($"{nama} berdasarkan tahun lahir anda tergolong Generasi Baby boomer");
    }
    else if(tahunLahir <= 1979)
    {
        Console.WriteLine($"{nama} berdasarkan tahun lahir anda tergolong Generasi Generasi X");
    }else if(tahunLahir <= 1994)
    {
        Console.WriteLine($"{nama} berdasarkan tahun lahir anda tergolong Generasi Generasi Y");
    }else if(tahunLahir <= 2015)
    {
        Console.WriteLine($"{nama} berdasarkan tahun lahir anda tergolong Generasi Generasi Z");
    }
    else
    {
        Console.WriteLine("terlalu muda");

    }
}

static void No6()
{
    Console.WriteLine("No 6");
    Console.Write("Nama : ");
    string nama = Console.ReadLine();
    Console.Write("Tunjangan : ");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Gapok : ");
    int gapok= int.Parse(Console.ReadLine());
    Console.Write("Banyak Bulan : ");
    int banyakBulan = int.Parse(Console.ReadLine());

    double pajak = 0;
    int gajiKotor = tunjangan + gapok;
    if (gajiKotor <=5000000)
    {
        pajak = 0.05;
    }
    else if(gajiKotor <= 10000000)
    {
        pajak = 0.1;
    }
    else
    {
        pajak=0.15;
    }

    float totalPajak = gajiKotor * (float)pajak;
    float bpjs = gajiKotor *3 / 100;
    float gajiPerBulan = gajiKotor -(totalPajak+bpjs);
    float totalGaji = gajiPerBulan * banyakBulan;

    // output
    Console.WriteLine("-------------");
    Console.WriteLine($"karyawan atas nama {nama} slip gaji sebagai berikut :");
    Console.WriteLine($"pajak : \t{totalPajak}");
    Console.WriteLine($"BPJS : \t{bpjs}");
    Console.WriteLine($"Gaji/Bulan : \t{gajiPerBulan}");
    Console.WriteLine($"Total Gaji/banyak Bulan : \t{totalGaji}");
      
}

static void No7()
{
    Console.WriteLine("Body Mass index");
    Console.Write("Masukkan berat Badan :");
    float berat = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Tinggi Badan : ");
    float tinggi = int.Parse(Console.ReadLine());
    tinggi = tinggi / 100;
    float bmi = berat / (tinggi * tinggi);
    Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi,4)}");
    if (bmi <= 18.5)
    {
        Console.WriteLine("Anda Termasuk berbadan Kurus");
    }
    else if (bmi <= 25)
    {
        Console.WriteLine("Anda Termasuk berbadan Langsing/sehat");
    }else
    {
        Console.WriteLine("Anda Termasuk berbadan gemuk");
    }
}
        

static void No8()
{
    Console.WriteLine("Nilai rata rata dan ketentuan lulus dan tidak");
    Console.Write("Masukkan Nilai MTK : ");
    int mtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Fisika : ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai kimia : ");
    int kimia = int.Parse(Console.ReadLine());
    float rataRata = ((float)mtk + (float)fisika + (float)kimia)/3;
    
    if(rataRata >= 50)
    {
        Console.WriteLine("Selamat");
        Console.WriteLine("Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }
    else
    {
        Console.WriteLine("Maaf");
        Console.WriteLine("Kamu Gagal");
    }

}



    
   

    