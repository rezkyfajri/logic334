﻿
using System.Collections.Immutable;
using System.ComponentModel.Design.Serialization;
using System.Text.RegularExpressions;
//No4 ();
//cobaNo5();
//dimPractice1();
dimPractice2();
Console.ReadKey();

static void No1()
{
    Console.WriteLine("Soal No 1");
    Console.Write("Masukkan Input (N) : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Awal : ");
    int nilaiAwal = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kelipatan : ");
    int kelipatan = int.Parse(Console.ReadLine());
    int tampung = Math.Abs(nilaiAwal);
    for (int i = 0; i < n; i++)
    {
        if(i== 0)
        {
            Console.Write(nilaiAwal + " ");
        }
        else {
            tampung += kelipatan;
            if (nilaiAwal >= 0) {

                if (i % 2 == 0)
                {
                    Console.Write(tampung + " ");
                }
                else
                {
                    Console.Write(tampung * -1 + " ");
                }        
            }
            else
            {
                if (i % 2 == 0)
                {
                    Console.Write(tampung * -1 + " ");
                }
                else
                {
                    Console.Write(tampung + " ");
                }   
            }          
        }     
    }
}


static void No2()
{
    Console.WriteLine("Soal Nomor 2");
   Console.Write("Masukan Input Waktu (hh:mm:dd(PM/AM)) : ");
    string input = Console.ReadLine().ToUpper();
    try
    {
        DateTime date1 = DateTime.ParseExact(input, "hh:mm:sstt", null);
        Console.WriteLine(date1.ToString("HH:mm:ss"));
    }
    catch (Exception e)
    {
        Console.WriteLine("Format yang dimasukkan salah");
        Console.WriteLine($"pesan eror : {e.Message}");
    }
}

static void No3() {

    Console.Write("Massukkan Kode Baju : ");
    int kodeBaju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Ukuran Baju : ");
    string ukuranBaju = Console.ReadLine().ToUpper();
    string merkBaju = "";
    if (kodeBaju == 1) 
    {
        merkBaju = "IMP";
        if (ukuranBaju == "S")
        {
            Console.WriteLine("Merk Baju = " +merkBaju);
            Console.WriteLine("Harga Baju = " + 200000.ToString("C2"));
        }
        else if (ukuranBaju == "M")
        {
            Console.WriteLine("Merk Baju = " +merkBaju);
            Console.WriteLine("Harga Baju : " + 220000.ToString("C2"));
        }
        else if (ukuranBaju == "XL" || ukuranBaju == "L" || ukuranBaju == "XXL")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 250000.ToString("C2"));
        }
        else
        {
            Console.WriteLine("Ukuran Tidak Ada");
        }
    }
    else if (kodeBaju ==2) {
        merkBaju = "Prada";
        if (ukuranBaju == "S")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 150000.ToString("C2"));
        }
        else if (ukuranBaju == "M")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 160000.ToString("C2"));
        }
        else if (ukuranBaju == "XL" || ukuranBaju == "L" || ukuranBaju == "XXL")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 170000.ToString("C2"));
        }
        else
        {
            Console.WriteLine("Ukuran Tidak Ada");
        }
    }
    else if (kodeBaju ==3) {
        merkBaju = "Gucci";
        if (ukuranBaju == "S")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 200000.ToString("C2"));
        }
        else if (ukuranBaju == "M")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 200000.ToString("C2"));
        }
        else if (ukuranBaju == "XL" || ukuranBaju == "L" || ukuranBaju == "XXL")
        {
            Console.WriteLine("Merk Baju = " + merkBaju);
            Console.WriteLine("Harga Baju : " + 200000.ToString("C2"));
        }
        else
        {
            Console.WriteLine("Ukuran Tidak Ada");
        }
    }

    else { Console.WriteLine("Kode Baju yang anda masukkan Salah"); }

}

static void No4()
{
    Console.WriteLine("Soal No 4");
    Console.Write("Masukkan Uang Andi : ");
    int uang = int.Parse(Console.ReadLine());
    Console.Write("Harga Baju(ex :35,40,50,20) : ");
    string[] hargaBaju = Console.ReadLine().Split(",");
    int[] hargaBajuInt = Array.ConvertAll(hargaBaju , int.Parse);
    Console.Write("Harga Baju(ex :40,30,45,10) : ");
    string[] hargaCelana = Console.ReadLine().Split(",");
    int[] hargaCelanaInt = Array.ConvertAll(hargaCelana , int.Parse);
    int tertinggi = 0;
    int total = 0;
    for (int i = 0; i < hargaBajuInt.Length; i++)
    {
        for (int j = 0; j < hargaCelanaInt.Length; j++)
        {
            total = hargaBajuInt[i] + hargaCelanaInt[i];
            if (uang >= total)
            {
                tertinggi = Math.Max(tertinggi, total);
            }
        }
    }

    Console.WriteLine(tertinggi);
}

static void No5()
{
    Console.WriteLine("Soal Nomor 5");
    Console.Write("arr (ex = 5,6,7,0,1): ");
    string[] input = Console.ReadLine().Split(",");
    Console.Write("rot : ");
    int rot = int.Parse(Console.ReadLine());
    
    for(int i = 1; i<=rot; i++)
    {
        
        Console.Write($"{i} : ");
        for (int j = i;j<input.Length; j++)
        {
            Console.Write(input[j] + ",");
        }
        for (int a = 0; a < i; a++)
        {
            Console.Write(input[a]);
            if (a != i - 1)
            {
                Console.Write(",");
            }
        }
        Console.WriteLine();
    }
}

static void cobaNo5()
{
    Console.WriteLine("Soal Nomor 5");
    Console.Write("arr (ex = 5,6,7,0,1): ");
    string[] input = Console.ReadLine().Split(",");

    Console.Write("rot : ");
    int rot = int.Parse(Console.ReadLine());
    string tampung = "";
    for (int i = 1; i <= rot; i++)
    {
        tampung = input[0];
        for (int j = 0; j < input.Length; j++)
        {
            input[j] = input[j + 1];
        }
        for (int tampilin = 0; tampilin < i; tampilin++)
        {
            Console.Write(input[tampilin]);
        }
        Console.WriteLine();

    }
}


static void No6()
{
    Console.WriteLine("Soal No 6 (Sorting)");
    Console.Write("Input : ");
    string[] input = Console.ReadLine().Split(",");
    int[] inputInt = Array.ConvertAll(input, int.Parse);
    // Array.Sort(input);
    
    int tampung = 0;
    for(int i = 0;i<inputInt.Length-1;i++)
    {
        for (int j = i+1; j < inputInt.Length; j++)
        {
            if (inputInt[j]< inputInt[i])
            {
                tampung = inputInt[i];
                inputInt[i] = inputInt[j];
                inputInt[j] = tampung;
            }
           // tampung = Math.Min(inputInt[i], inputInt[j]);
        }
    }
    string output = string.Join((","), inputInt);  
    Console.WriteLine(output);
}

static void dimPractice1()
{
    Console.WriteLine("Dim Practice 1");
    Console.WriteLine("membuat perulangan soal 1 - 4");
    Console.Write("masukkan Nilai awal : ");
    int nilaiAwal = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Banyak bilangan yang akan dicetak : ");
    int panjangBilangan = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Kelipatan : ");
    int kelipatan = int.Parse(Console.ReadLine());
    int nilaiAkhir = panjangBilangan * kelipatan;

    while (nilaiAkhir >= nilaiAwal)
    {
        Console.Write(nilaiAwal + " ");
        nilaiAwal = nilaiAwal + kelipatan;
    }
}

static void dimPractice2()
{

    Console.WriteLine("Dim Practice 1");
    Console.WriteLine("membuat perulangan soal 5");
    Console.WriteLine("1 5 * 9 13 * 17");

    Console.Write("masukkan Nilai awal : ");
    int nilaiAwal = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Banyak bilangan yang akan dicetak : ");
    int panjangBilangan = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Kelipatan : ");
    int kelipatan = int.Parse(Console.ReadLine());
    for (int i = 1; i <= panjangBilangan; i++)
    {
        int mod = i % 3;
        if (mod == 0)
        {
            Console.Write("* ");
            continue;

        }
        Console.Write(nilaiAwal + " ");
        nilaiAwal += kelipatan;
    }
}