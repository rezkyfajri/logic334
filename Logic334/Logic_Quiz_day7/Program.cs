﻿using System.Numerics;
using System.Runtime.InteropServices;
using System.Xml.Schema;

No8();
Console.ReadKey();

static void No1()
{
    Console.WriteLine("Soal NO 1 Tentang Faktorial");
    Console.Write("X = ");
    int x = int.Parse(Console.ReadLine());
    
    int[] faktorial = new int[x];
    int index=0;
    int hasil = 0;
    for (int i = x; i > 0; i--)
    {
        faktorial[index] = i;
        hasil += faktorial[index];
        index++;       
    }
    Console.Write($"{x}! = ");
    Console.WriteLine(string.Join(" X ", faktorial)+ " = " +hasil);
    Console.Write($"Ada {hasil} Cara" );
}

static void No2()
{
    Console.WriteLine("Soal Nomor 2");
    Console.Write("input : ");
    string input = Console.ReadLine().ToUpper();
    //char[] input1 = input.ToCharArray();
    int count = 0;
    if( input.Length % 3 == 0 ) {

        for (int i = 0; i < input.Length; i += 3)
        {
            if ("SOS" != input.Substring(i,3))
            {
                count += 1;
            }
        }
        Console.WriteLine("Total Sinyal Yang salah : " + count);
    }
    else
    {
        Console.WriteLine("Inputan Kurang");
    }
    

    
}

static void No3()
{
    Console.WriteLine("Sola Nomor 3 tentang denda pengembalian buku");
    Console.Write("Masukkan Tanggal Peminjaman Buku : ");
    string tglPeminjaman = Console.ReadLine();

    Console.Write("Masukkan Tanggal pengembalian Buku : ");
    string tglPengembalian = Console.ReadLine();

    try
    {
        DateTime peminjaman = DateTime.ParseExact(tglPeminjaman, "d-M-yyyy", null);
        DateTime pengembalian = DateTime.ParseExact(tglPengembalian, "d-M-yyyy", null);
        
        TimeSpan interval = pengembalian - peminjaman;
        int keterlambatan = int.Parse(interval.Days.ToString());
        if(keterlambatan <= 3 )
        {
            Console.WriteLine($"Tidak Terlambat dan tidak bayar");
        }
        else
        {
            int totalDenda = (keterlambatan - 3) * 500;
            Console.WriteLine($"Lama Keterlambatan Pengambalian : {keterlambatan}");
            Console.WriteLine("Denda yang Harus Dibayar = " + totalDenda.ToString("C2"));
        }
        

    }
    catch(Exception e)
    {
        Console.WriteLine("Type memasukkan tgl anda salah");
        Console.WriteLine("eror " +e.Message);
    }
}

static void No4()
{
    Console.WriteLine("Soal No 4");
    Console.Write("Masukkan tanggal mulai (dd/MM/yyy) : ");
    string input = Console.ReadLine();
    try
    {
        DateTime dt = DateTime.ParseExact(input, "d/M/yyyy", null);
        Console.Write("Mas ukkan Hari akan Dilaksanakannya Ft1 : ");
        int hariFt1 = int.Parse(Console.ReadLine());
        Console.Write("Masukkan tgl Hari Libur : ");
        string[] hariLibur = Console.ReadLine().Split(",");
        int[] hariLiburInt = Array.ConvertAll(hariLibur, int.Parse);
        //int totalHariLibur = hariLiburInt.Length;

        for (int i = 0; i < hariFt1; i++)
        {
            if((int)dt.DayOfWeek == 0 || (int)dt.DayOfWeek == 6)
            {
                hariFt1 += 1;
            }
            else
            {
                for (int j = 0; j < hariLiburInt.Length; j++)
                {
                    if (hariLiburInt[j] == dt.Day)
                    {
                        hariFt1 += 1;
                    }
                }
            }
            dt = dt.AddDays(1);
        }
        Console.WriteLine("Kelas Akan Ujian Pada : "+dt.ToString("dd/MM/yyyy"));
    }
    catch(Exception e)
    { Console.WriteLine(e.Message); 
    }

}

static void No5()
{
    Console.WriteLine("Soal Nomor 5 tentang Huruf vokal dan kosntanta");
    Console.Write("Masukkan input : ");
    char[] input = Console.ReadLine().ToUpper().ToCharArray(); ;

    int vokal = 0;
    int konsonan = 0;
    for(int i = 0; i < input.Length; i++)
    {
        //char[] chars = input[i].ToCharArray();
        
        //for (int j = 0; j < chars.Length; j++)
        //{
            if (char.IsLetter(input[i]))
            {
                if (input[i] == 'A' || input[i] == 'I' || input[i] == 'U' || input[i] == 'E' || input[i] == 'O')
                {
                    vokal++;
                }
                else

                {
                    konsonan++;
                }
            }    
       // }
    }
    Console.WriteLine($"Jumlah Huruf Vokal : {vokal}");
    Console.WriteLine($"Jumlah Huruf konsonan : {konsonan}");
}
  static void No6()
{
    Console.WriteLine("Soal Nomer 6");
    Console.Write("Masukkan Input nama : ");
    string input = Console.ReadLine().ToLower();
    for (int i = 0;i<input.Length; i++)
    {
        for (int j = 0; j < input.Length; j++)
        {
            if (j== input.Length/2)
            {
                if ((input.Length/ 2) %2 == 0)
                {
                    Console.Write($"{input[i]}*");
                }
                else
                {
                    Console.Write($"{input[i]}");
                }             
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.WriteLine();
    }
}

static void No7()
{
    Console.WriteLine("Soal Nomer 7 Tentang Total pembayaran dan sisa uang");
    Console.Write("Masukkan Total Menu : ");
    int totalMenu = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Index Makanan Alergi : ");
    int indexMakananAlergi = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Harga menu : ");

    string[] hargaMenu = Console.ReadLine().Split(",");
    int[] hargaMenuInt = Array.ConvertAll(hargaMenu, int.Parse);
    int totalHarga = 0;
    if (hargaMenuInt.Length-1 > indexMakananAlergi && hargaMenuInt.Length == totalMenu)
    {
        Console.Write("Masukkan Uang Elsa : ");
        int uangElsa = int.Parse(Console.ReadLine());
        //nambahin total harga
        for (int i = 0; i < hargaMenuInt.Length; i++)
        {
            if (i == indexMakananAlergi)
            {
                continue;
            }
                totalHarga += hargaMenuInt[i];          
        }
        totalHarga = totalHarga / 2;
        int sisa = uangElsa - totalHarga;
        if(sisa > 0)
        {
            Console.WriteLine($"Sisa Uang Elsa : {sisa}");
        }
        else if (sisa < 0)
        {
            Console.WriteLine($"Uang Elsa Kurang : {sisa}");
        }
        else
        {
            Console.WriteLine("Uang Elsa Pas");
        }

    }
    else
    {
        Console.WriteLine("Index makanan Tidak ditemukan atau harga yang dimasukkan tidak sesuai dengan total menu");
    }

}

static void No8()
{
    /*   Console.WriteLine("Soal Nomer 8");
       Console.WriteLine("Membuat segitiga siku siku");
       Console.Write("Masukkan Input : ");
       int input = int.Parse(Console.ReadLine());*/
    int x = 5;
    for (int i = 0; i > 3; i--)
    {
        for(int j = 0; j < 2; j++)
        {
            Console.Write(j);
        }
        /*for(int a = 0; a <=i; a++)
        {
            Console.Write("#");
        }*/
        Console.WriteLine();
    }
}
static void No9()
{
    Console.Write("Masukkan ukuran matrix  : ");
    int panjang = int.Parse(Console.ReadLine());
    int[,] matrix = new int[panjang-1, panjang-1];
    int dia1 = 0;
    int dia2 = 0;
    for(int i = 0; i< panjang; i++)
    {
        for(int j =0; j < panjang; j++)
        {
            Console.Write($"input {i} {j} : ");
            matrix[i,j] = int.Parse(Console.ReadLine());          
        }
    }
    Console.WriteLine(dia1);
    Console.WriteLine(dia2);
    Console.WriteLine(dia1-dia2);
}
static void No10()
{
    Console.WriteLine("Soal No 10 tentang Mencari angka tertinggi dan jumlah angka tertinggi");
    Console.Write("Masukkan Input angka : ");
    string[] input = Console.ReadLine().Split(" ");
    int[] inputInt = Array.ConvertAll(input, int.Parse);
    int nilaiMax = 0;
    int count = 0;
    for (int i = 0; i < inputInt.Length; i++)
    {
        nilaiMax = Math.Max(nilaiMax, inputInt[i]);
    }
    for(int i = 0;i < input.Length; i++) 
    {
        if(nilaiMax == inputInt[i])
        {
            count++;
        }
    }

    Console.WriteLine("Nilai tertinggi berjumlah : "+count);
}