﻿//OperatorAritmatika();
//OperatorPenugasan();

//OperatorPerbandingan();
//OperatorLogika();
MethodReturnType();
Console.ReadKey();


static void MethodReturnType()
{
    Console.WriteLine("Cetak  method return Type");
    Console.Write("masukkan mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("masukkan apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);
    Console.WriteLine("hasil mangga + Apel = " + jumlah);
}

static int hasil(int mangga, int apel)
{
    int hasil;
    hasil = mangga + apel;
    return hasil;

}



static void OperatorLogika()
{
    Console.WriteLine("Operator Perbandingan ");
    Console.Write("masukkan Umur : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("masukkan Pasword : ");
    string pasword = Console.ReadLine();


    bool isAge = age > 18;
    bool isPasword = pasword == "admin";

    if (isAge && isPasword)
    {
        Console.WriteLine("Welcome to ....");

    } 
    else
    {
        Console.WriteLine("Sorry, tidak memenuhi kriteria");
    }
}


static void OperatorPerbandingan()
{
    int mangga, apel = 0;
    Console.WriteLine("---Operator Perbandingan---");
    Console.Write("masukkan jumlah mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("masukkan jumlah apel : ");
    apel = int.Parse(Console.ReadLine());


    Console.WriteLine("Hasil Perbandingan");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
}


static void OperatorPenugasan()
{
    int apel = 6;
    Console.WriteLine($"jumlah apel awal {apel}");
    Console.Write("masukkan tambahan apel = ");
    apel += int.Parse(Console.ReadLine());
    Console.WriteLine($"jumlah apel menjadi {apel}");
}

static void OperatorModulus()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("Operator aritmatika");
    Console.Write("imput mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("input apel : ");
    apel = Convert.ToInt32(Console.ReadLine());

    hasil = apel % mangga;
    Console.WriteLine("hasil dari {0} % {1} adalah {2}", mangga, apel, hasil);
}

static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("Operator aritmatika");
    Console.Write("imput mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("input apel : ");
    apel = Convert.ToInt32(Console.ReadLine());

    hasil = apel + mangga;
    Console.WriteLine("hasil dari {0} + {1} adalah {2}",mangga, apel, hasil);
}

static void Konversi()
{
    //untuk input selain string harus konveksi dulu
    int myInt = Convert.ToInt32(Console.ReadLine());
    double mydouble = Convert.ToDouble(Console.ReadLine());

    //cara konvert ke string
    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();

    string strMyInt3 = mydouble.ToString();
    int strMyInt4 = Convert.ToInt32(mydouble);
    double strMyInt5 = Convert.ToDouble(myInt);



    Console.WriteLine(myInt);
    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(strMyInt3);
    Console.WriteLine(strMyInt4);
    Console.WriteLine(strMyInt5);
}
