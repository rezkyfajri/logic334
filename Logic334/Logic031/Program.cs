﻿
//PerulanganDoWhile();
//perulanganFor();
//perulanganForEach();
//penggunaanLength();
//removeString();
//insertString();
//replaceString();
//splitDanJoin();
subString();
//containsString();
//convertAll();


//penggunaanToCharArray();
Console.ReadKey();
static void PerulanganWhile()
{
    Console.WriteLine("perulangan while");
    Console.Write("masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (nilai <10)
    {
        Console.WriteLine(nilai);
        nilai++;
    }
}


static void PerulanganDoWhile()
{
    Console.WriteLine("perulangan Do while");
    Console.Write("masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 10);
    
    }

static void perulanganFor()
{

    Console.WriteLine("Perulangan for");
    Console.Write("Masukkan Inputitinggi segitiga : ");
    int input = int.Parse(Console.ReadLine());

    for (int i = 0; i <= input; i++)
    {
        for(int a=i;a<=input;a++)
        {
            Console.Write(" ");
        }
        for (int j = 0; j <= i; j++)
        {
            Console.Write("* ");
        }
        Console.WriteLine();
    }
}

static void perulanganForEach()
{
    int[] namaArray = { 4, 7, 13, 44, 15 };
    int total = 0;
    foreach (int n in namaArray)
    {

        total += n;
    }
    Console.WriteLine("jumlah total array " + total);

}

static void penggunaanLength()
{
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.WriteLine(kata.ToUpper());
    Console.WriteLine(kata.ToLower());
    Console.WriteLine(kata.Length);
}

static void removeString()
{
    Console.WriteLine("remove String");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukkan Index remove : ");
    int index = int.Parse(Console.ReadLine());
    Console.WriteLine($"dan kata menjadi {kata.Remove(index)}");
}

static void insertString()
{
    Console.WriteLine("remove String");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukkan Index insert : ");
    int index = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kata yang akan diinsert : ");
    string insertKata = Console.ReadLine();
    Console.WriteLine($"dan kata menjadi {kata.Insert(index,insertKata)}");
}
static void replaceString()
{
    Console.WriteLine("remove String");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukkan Kata yang akan di replace : ");
    string kataLama = Console.ReadLine();
    Console.Write("Masukkan Kata baru : ");
    string kataBaru = Console.ReadLine();
    Console.WriteLine($"dan kata menjadi {kata.Replace(kataLama, kataBaru)}");
}

static void splitDanJoin()
{
    Console.WriteLine("Split dan Join");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan Type Split nya : ");
    string split = Console.ReadLine();

    string[] kataKata = kalimat.Split(split);
    foreach(string kata in kataKata)
    {
        Console.WriteLine(kata);
    }
    
    Console.WriteLine(string.Join(" + ", kataKata));
}

static void subString()
{
    Console.WriteLine("sub string");
    Console.Write(" Masukkan Kode : ");
    string kode = Console.ReadLine();
    Console.Write("Masukkan Parameter 1 : ");
    int param1 = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Parameter 2 : ");
    int param2 = int.Parse(Console.ReadLine());
    if (param2 == 0)
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param1)}");
    }
    else
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param1, param2)}");
    }
}

static void containsString()
{
    Console.WriteLine("contain string");
    Console.Write(" Masukkan Kode : ");
    string kata = Console.ReadLine();
    Console.Write(" Masukkan Kode : ");
    string contain = Console.ReadLine();
    if (kata.Contains(contain))
    {
        Console.WriteLine("true, kata " + contain + " terdapat dalam " +kata);
    }
    else
    {
        Console.WriteLine("false kata " + contain + " tidak terdapat dalam " + kata);
    }
}

static void penggunaanToCharArray()
{
    Console.WriteLine("mengubah string menjadi array");
    Console.Write("masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    char[] hasil = kalimat.ToCharArray();
    foreach (char c in hasil)
    {
        Console.WriteLine(c);
    }
}

static void convertAll()
{
    Console.WriteLine("Convert All");
    Console.Write("Masukkan Input Angka (Pisahkan Dengan Spasi): ");
    string[] input = Console.ReadLine().Split(" ");

    int jumlah = 0;
    int[] array = Array.ConvertAll(input, int.Parse);

    foreach(int i in array)
    {
        jumlah += i;
    }

    Console.WriteLine("jumlah : "+jumlah);


}
