﻿
padLeft();
//rekursif();
Console.ReadKey();

static void rekursif()
{
    Console.WriteLine("Rekursif fungtion");
    Console.WriteLine("fungsi yang memanggil dirinya sendiri");
    Console.Write("Masukkan Input Awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Input Akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Typer (ASC/DESC) : ");
    string type = Console.ReadLine().ToUpper();

    Perulangan(start, end, type);
}


static int Perulangan (int start, int end, string type)
{


    if (start == end)
    {
        Console.WriteLine(type == "ASC" ? start : end);
        return 0;
    }
    if (type == "ASC")
    {
        Console.WriteLine(start);
        return Perulangan(start + 1, end, type);
    }
    else
    {
        Console.WriteLine(end);
        return Perulangan(start , end -1, type);
    }

    
}



static void padLeft()
{
    Console.WriteLine("Pad Left dan pad right");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Panjang Karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Karakter : ");
    char chars = char.Parse(Console.ReadLine());
    
    //output 23110001 
    //2 digit tahun, 2 digit bulan , generate length

    DateTime date =DateTime.Now;

    string code = "";

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);
    //code = date.ToString("yyMM") + input.ToString().PadRight (panjang, chars);


    Console.WriteLine(code);
}